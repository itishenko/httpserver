package com.splinex;//package com.splinex;

import com.splinex.http.FileHandler;
import com.splinex.http.HttpServer;
import com.splinex.http.InstallHandler;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int port;
        if (args.length != 0)
            port = Integer.parseInt(args[0]);
        else
            port = 1032;
        System.out.print("\nport=" + port + "\n");
        HttpServer s = new HttpServer(port);
        s.registerHandler(new InstallHandler());
        FileHandler fh = new FileHandler();
        try {
            s.start();
            while(true)
            {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

