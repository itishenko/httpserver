package com.splinex.http;

//import android.content.Context;
//import android.content.res.AssetManager;
//import android.webkit.MimeTypeMap;
//import com.splinex.streaming.Log;
import fi.iki.elonen.NanoHTTPD;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class HttpFileHandler extends RequestHandler {
    public static final String HTTP_ROOT = "http_root";
    public static final String UID = "uid";
    private static MyLogger log = new MyLogger();
    public final String fileOutputName;

    public HttpFileHandler() {
        super("");
        fileOutputName = HttpFileHandler.class.getName();
    }

    @Override
    public NanoHTTPD.Response handle(NanoHTTPD.IHTTPSession session) {
        String filename = session.getUri();
        log.open(fileOutputName);
//        AssetManager assets = context.getAssets();
//        String sQPS = session.getQueryParameterString();
//        String sHdr = session.getHeaders().toString();
//        String sMtd = session.getMethod().toString();
//        String sInpStr = session.getInputStream().toString();
//        String sPrm = session.getParms().toString();
//        String sUri = session.getUri();
//        log.info("\nFH\n\nQueryParameterString:\n" + sQPS
//                + "\n\nHeaders:\n" + sHdr
//                + "\n\nMethod\n" + sMtd
//                + "\n\nInputStream\n" + sInpStr
//                + "\n\nParams\n" + sPrm
//                + "\n\nUri\n" + sUri
//                + "\n\n" + session.getCookies().toString()
//                + "\n\n" + session.toString());
        try {
//            InputStream is = assets.open(HTTP_ROOT + filename);
            if (session.getMethod() == NanoHTTPD.Method.GET && session.getUri().equals("/" + HttpServer.STATE_INFO)) {
                if (session.getHeaders().get(UID) != null) {//
                    String uid = session.getHeaders().get(UID);
                    String statusFileName = findFileStatus(HTTP_ROOT, uid);
                    log.close(fileOutputName);
                    return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK,
                            HttpServer.MIME_PLAINTEXT, "file with uid=" + uid);
                }
                else {// get full list
                    String statusFileName = findFilesStatus(HTTP_ROOT);
                    log.close(fileOutputName);
                    return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK,
                            HttpServer.MIME_PLAINTEXT, statusFileName);
                }
            }
            else {// saving file
                InputStream is = new FileInputStream(HTTP_ROOT + filename);
                String mime = getMime(filename);
                log.warning(filename + " => " + mime);
                log.close(fileOutputName);
                return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, mime, is);
            }
        } catch (FileNotFoundException e) {
            log.severe("File not found: " + filename);
        } catch (IOException e) {
            log.severe("IOException: " + e);
        } catch (Exception e) {
            log.severe("IOException: " + e.getMessage());
        }

        log.close(fileOutputName);
        return null;
    }

    private String findFilesStatus(String httpRoot) {
        String result = "";
        File f = new File(httpRoot);
        String[] list = f.list();     //список файлов в текущей папке
        for (String file : list) {
            result += file + "//";
        }
        log.info(result);
        return result;
    }

    private String findFileStatus(String httpRoot, String uid) {
        String fileName = HttpServer.uid_fileName.get(uid);
        String fileStatus = "";
        if (fileName != null) {
            String[] parts = fileName.split(HttpServer.SEPARATOR);
            fileName = parts[0];
            fileStatus = parts.length > 1 ? parts[1] : "3";
        }
        return fileStatus;
    }

    private String getMime(String filename) {
        int idx = filename.lastIndexOf(".");
        String ext = filename.substring(idx + 1);
        return "js".equals(ext) ? "text/javascript" :
                "text/plain";
    }
}