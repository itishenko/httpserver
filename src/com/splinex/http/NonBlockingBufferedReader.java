package com.splinex.http;

import java.io.BufferedReader;

/**
 * Created by human on 4/16/15.
 */
public class NonBlockingBufferedReader extends Thread {
    private MyLogger log;
    private String logName = "MyLog_NonBlockingBufferedReader.txt";
    private BufferedReader in;
    private boolean finished;

    public NonBlockingBufferedReader(String _logName, BufferedReader _in) {
        logName = _logName;
        in = _in;
    }

    @Override
    public void run()	//Этот метод будет выполнен в побочном потоке
    {
        finished = false;
        log = new MyLogger();
        log.open(logName);
        try {
            String line;
            while ((line = in.readLine()) != null) {
                log.script(line);
            }
        } catch (Exception e) {
            System.out.println("NonBlockingBufferedReader: error" + e.getMessage());
        }
        log.close();
//        System.out.println("NonBlockingBufferedReader: out");
        finished = true;
    }

    public boolean isFinished () {
        return finished;
    }
}
