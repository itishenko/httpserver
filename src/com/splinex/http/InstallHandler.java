package com.splinex.http;

import fi.iki.elonen.NanoHTTPD;

import java.io.*;
//import java.net.URI;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.json.JSONTokener;
//import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

//import java.nio.file.StandardCopyOption;


public class InstallHandler extends RequestHandler {

    public static final String FILE = "file";
    public static final String FILENAME = "file";
    public static final String HTTP_ROOT = "http_root/";
    public static final String CHUNKS_SUFFIX = ".chunks";
    public static final String UID = "uid";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String PROFILE = "profile";
    public static final String STATE_OPTION = "type";
    public static final int WAITING_STATE = 5;
    public static final String ERROR_CODE = "errorCode";
    public static final int SERVER_ERROR = 0;
    public static final int RUN_EXECUTING_ERROR = 0;
    public static final int SERVER_STITCHING_ERROR = 2;
    public static final String PERCENT_OPTION = "percent";
    public static final String DESCRIPTION_OPTION = "description";
    private final MyLogger log;
    private static MyLogger log_time_report = new MyLogger();
    public String outputLogName;
    public static ExecutorService executor;
    public String uidCut;

    public InstallHandler() {
        super("/install");
//        log.open(HttpFileHandler.class.getName());
        executor = Executors.newSingleThreadExecutor();//Executors.newFixedThreadPool(2);
        log_time_report.open(HTTP_ROOT + "time_report.txt");
        log = new MyLogger();
    }

    @Override
    public NanoHTTPD.Response handle(NanoHTTPD.IHTTPSession session) {
        boolean result = true;
        String r = "";
        if (session.getMethod() == NanoHTTPD.Method.POST) {
            Map<String, String> files = new HashMap<String, String>();
            try {
                session.parseBody(files);
//                log.info(files.size());
                String fileName = session.getParms().get(FILE);
                String folderToSave = HTTP_ROOT + getFolder(fileName);
                String[] parts = fileName.split("_");
                if (!fileName.endsWith(CHUNKS_SUFFIX))
                    outputLogName = HTTP_ROOT + parts[0] + CHUNKS_SUFFIX + ".txt";
                else
                    outputLogName = HTTP_ROOT + fileName + ".txt";
                log.open(outputLogName);
                log.info("handle thread started\n");
                File dir = new File(HTTP_ROOT);
                if (result && !dir.exists()) {
                    if (!dir.mkdirs()) {
                        result = false;
                        log.severe("\n\ncan't create folder " + HTTP_ROOT);
                    }
                }
                if (result) {
                    FileChannel in = new FileInputStream(new File(files.get(FILE))).getChannel();
                    FileChannel out = new FileOutputStream(new File(folderToSave)).getChannel();
                    result = out.transferFrom(in, 0, in.size()) > 0;
                    out.close();
                }

                if (fileName.endsWith(CHUNKS_SUFFIX) && result) {
                    JSONObject jobj = getJsonObjectFromFile(HTTP_ROOT + fileName, log);
                    String uid = "";
                    String title = "";
                    String description = "";
                    String profile = "";
                    uidCut = "-1";
                    if (jobj != null) {
                        uid = jobj.optString("uid");
                        title = jobj.optString("title");
                        description = jobj.optString("description");
                        profile = jobj.optString("profile");
                        if (uid != null)
                            uidCut = uid.substring(0, Math.min(uid.length(), 10)) + "...";
//                        System.out.println("4" + jobj.get("uid"));
//                        System.out.println("=" + uid);
                    }
//                    uid = session.getHeaders().get(UID);
//                    title = session.getHeaders().get(TITLE);
//                    description = session.getHeaders().get(DESCRIPTION);
//                    profile = session.getHeaders().get(PROFILE);
                    log.info("uid,title,descr,prof:1 " + uid + "," + title + "," + description + "," + profile);
                    uid = uid != null ? uid : "-1";
                    title = title != null ? title : "Empty_title";
                    description = description != null ? description : "Empty_description";
                    profile = profile != null ? profile : "Empty_profile";
                    log.info("uid,title,descr,prof:2 " + uid + "," + title + "," + description + "," + profile);
                    uid = uid.length() != 0 ? uid : "-1";
                    title = title.length() != 0 ? title : "Empty_title";
                    description = description.length() != 0 ? description : "Empty_description";
                    profile = profile.length() != 0 ? profile : "Empty_profile";
                    log.info("uid,title,descr,prof:3 " + uid + "," + title + "," + description + "," + profile + "\n");
                    r = uid + " " + title + " " + description;
                    log_time_report.info(uidCut + ":start handling " + uid);
                    String outputFile = "_" + fileName;
                    String command = "./run.py" +
                            " -i " + HTTP_ROOT + fileName +
                            " -d " + HTTP_ROOT +
                            " -o " + HTTP_ROOT + outputFile +
                            " -n " + title +
                            " -t " + description;
//                    log.info("command=" + HTTP_ROOT + fileName + "\n");
                    consoleRun(folderToSave, uid, title, description, profile, command, fileName, outputFile, uidCut);
                }
//                result = new Installer(files.g).install(files.get(FILE));
            } catch (Exception e) {
                result = false;
//                e.printStackTrace();
                // log.severe("Exception:" + e.printStackTrace());
                log.severe(e.getMessage());
            }

        }
        log.info("handle thread at ending\n");
        log.close();
//form input type upload
        return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, HttpServer.MIME_PLAINTEXT,
                (result ? "file saved" : "saving file error") + " " + r);
    }

    private void consoleRun(String folderToSave, String uid,
                            String title, String description, String profile,
                            String command, String fileName, String outputFile, String uidCut) {
//        Process p=Runtime.getRuntime().exec("nautilus");
        HashMap<String, Integer> pairs = new HashMap<String, Integer>();
        pairs.put(STATE_OPTION, WAITING_STATE);
        saveByJsonToFile(pairs, HTTP_ROOT + uid + ".txt", log);

        JSONObject obj = getJsonObjectFromFile(HTTP_ROOT + fileName, log);
        if (obj != null) {
            log_time_report.info(uidCut + ":cameraUploadTime: " + obj.optString("cameraUploadTime", "null"));
            log_time_report.info(uidCut + ":serverUploadTime: " + obj.optString("serverUploadTime", "null"));
        }

        ConsoleThread ct = new ConsoleThread(folderToSave, uid, title, description, profile, command,
                fileName, outputFile, outputLogName, uidCut);
//        ct.setParams(folderToSave, uid, title, description, profile, command, fileName, outputFile, outputLogName);
        executor.execute(ct);
//        ct.start();
    }

    private void saveByJsonToFile(HashMap<String, Integer> pairs, String fileName, MyLogger log) {
        JSONObject obj = new JSONObject();
        for (String key : pairs.keySet()) {
            obj.put(key, pairs.get(key));
        }

//            JSONArray company = new JSONArray();
//            company.put("Compnay: eBay");

//            obj.put("Company List", company);

        try {
            FileWriter file = new FileWriter(fileName, false);
            file.write(obj.toString());
            log.info("Successfully Copied JSON Object to File...");
            log.info("JSON Object: " + obj);
            file.flush();
            file.close();
        } catch (IOException e) {
            log.severe("saveByJsonToFile11: " + e.getMessage());
        } catch (Exception e) {
            log.severe("saveByJsonToFile12: " + e.getMessage());
        }
    }

    private void saveByJsonToFile(JSONObject obj, String fileName, MyLogger log) {
        try {
            FileWriter file = new FileWriter(fileName, false);
            file.write(obj.toString());
            log.info("Successfully Copied JSON Object to File...");
            log.info("JSON Object: " + obj);
            file.flush();
            file.close();
        } catch (IOException e) {
            log.severe("saveByJsonToFile21: " + e.getMessage());
        } catch (Exception e) {
            log.severe("saveByJsonToFile22: " + e.getMessage());
        }
    }

    private JSONObject getJsonObjectFromFile(String fileName, MyLogger log) {
        JSONObject root;
        try {
            FileReader fr = new FileReader(fileName);
            JSONTokener tokener = new JSONTokener(fr);
            root = new JSONObject(tokener);
        }
        catch (Exception e) {
            log.severe(e.getMessage() + " in file: " + HTTP_ROOT + fileName);
//            System.out.println(e.getMessage());
//            e.printStackTrace();
            return null;
        }
        return root;
    }

    private boolean writeToFile(String fileName, String text) {
        FileWriter writeFile = null;
        try {
            File logFile = new File(fileName);
            writeFile = new FileWriter(logFile, false);
            writeFile.write(text);
            writeFile.close();
            return true;
        } catch (Exception e) {
            log.severe(e.getMessage());
            return false;
        }
    }

    private boolean AppendLineToFile(String fileName, String text) {
        try
        {
            FileWriter fw = new FileWriter(fileName,true); //the true will append the new data
            fw.write(text + "\n");//appends the string to the file
            fw.close();
            return true;
        }
        catch(IOException ioe)
        {
            log.severe("IOException: " + ioe.getMessage());
            return false;
        }
    }

    class ConsoleThread extends Thread
    {
        private String folderToSave;
        private final String uid;
        private String title;
        private String description;
        private String profile;
        private String command;
        private final String fileName;
        private final String outputFileName;
        public final String outputLogName;
        private MyLogger log;
        private final String uidCut;
        double lastPercent;
        java.util.Date last_date;

        public ConsoleThread(String _folderToSave,
                             String _uid,
                             String _title,
                             String _description,
                             String _profile,
                             String _command,
                             String _fileName,
                             String _outputFile,
                             String _outputLogName,
                             String _uidCut)
        {
            this.folderToSave = _folderToSave;
            uid = _uid;
            title = _title;
            description = _description;
            profile = _profile;
            command = _command;
            fileName = _fileName;
            outputFileName = _outputFile;
            outputLogName = _outputLogName;
            uidCut = _uidCut;
//            setParams(_folderToSave,
//                _uid,
//                _title,
//                _description,
//                _profile,
//                _command,
//                _fileName,
//                _outputFile,
//                _outputLogName);
        }

        @Override
        public void run()	//Этот метод будет выполнен в побочном потоке
        {
            log = new MyLogger();
            log.open(outputLogName);
            log.info("children thread for running of run.py started\n");
            try {
//                OutputStream rtm = Runtime.getRuntime().exec(command, null).getOutputStream();
//                PrintStream prtStrm = new PrintStream(rtm);
//                prtStrm.println();

//                ProcessBuilder pb = new ProcessBuilder(command);
//                pb.redirectOutput(Redirect.INHERIT);
//                pb.redirectError(Redirect.INHERIT);
//                Process p = pb.start();
//

//                log.info("\ncommand1:" + command + "\n");
//                Process p = Runtime.getRuntime().exec("java -version", null);
////                Process p = Runtime.getRuntime().exec("./concat.py");//echo "hello" > f.txt//mkdir aqqqqq
////                OutputStream os = p.getOutputStream();
////                log.info(os.write(); + "\n");
////                PrintStream prtStrm = new PrintStream(p.getOutputStream());
////                prtStrm.println();
////                log.info(p.getOutputStream());
//                BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
//                String line;
//                while ((line = input.readLine()) != null) {
//                    log.info(line);
//                }
//                int r = p.waitFor();

//
//                log.info("\ncommand2:" + "./Allie -i " + outputFileName + " -o" + outputFileName + "1");
//                p = Runtime.getRuntime().exec("./Allie -i " + outputFileName + " -o " + outputFileName + "1", null);
//                int res = p.waitFor();
//                PrintStream prtStrm1 = new PrintStream(p.getOutputStream());
//                prtStrm1.println();
//
//                log.info("\ncommand3:" + "./360VideosMetadata.py -i " + outputFileName + "1 " + outputFileName + "2");
//                p = Runtime.getRuntime().exec("./360VideosMetadata.py -i " + outputFileName + "1 " + outputFileName + "2", null);
//                res = p.waitFor();
//                PrintStream prtStrm2 = new PrintStream(p.getOutputStream());
//                prtStrm2.println();
//
//                log.info("\ncommand4:" + "./youtubeworker --upload " + outputFileName + "2 --video-description \"1111\" --video-title \"yyy\"");
//                p = Runtime.getRuntime().exec("./youtubeworker --upload " + outputFileName + "2 --video-description \"1111\" --video-title \"yyy\"", null);
//                res = p.waitFor();
//                PrintStream prtStrm3 = new PrintStream(p.getOutputStream());
//                prtStrm3.println();
//
//                String argArray[] = {
//                        "-i", HTTP_ROOT + fileName,
//                        "-d", HTTP_ROOT,
//                        "-o", HTTP_ROOT + outputFileName,
//                        "-n", title,
//                        "-t", description};
//

                boolean runByRunDotPy = true;


                if (runByRunDotPy) {
                    String argArray[] = {
                             "-i", fileName
                            ,"-d", HTTP_ROOT
//                            ,"-o", HTTP_ROOT + outputFileName
//                            ,"-n", title
//                            ,"-t", description
//                            ,"-p", profile
//                            ,"-u", uid
                    };
                    List<String> cmdList = new ArrayList<String>();
                    cmdList.add("./run.py");
                    for (String arg : argArray)
                        cmdList.add(arg);

                    runProcessViaProcessBuilder(cmdList);
//                    runProcessViaRuntimeExec(cmdList);
                }
                else {

                    String argArray[] = {
                            "-i", HTTP_ROOT + fileName,
                            "-d", HTTP_ROOT,
                            "-o", HTTP_ROOT + outputFileName
                    };
                    List<String> cmdList = new ArrayList<String>();
                    cmdList.clear();
                    cmdList.add("./concat.py");
                    for (String arg : argArray)
                        cmdList.add(arg);
                    runProcessViaProcessBuilder(cmdList);

                    String argArray2[] = {
                            "-i", HTTP_ROOT + outputFileName,
                            "-o", HTTP_ROOT + "_" + outputFileName
                    };
                    cmdList.clear();
                    cmdList.add("./Allie");
                    for (String arg : argArray2)
                        cmdList.add(arg);
                    runProcessViaProcessBuilder(cmdList);

                    String argArray3[] = {
                            "-i", HTTP_ROOT + "_" + outputFileName,
                            HTTP_ROOT + "__" + outputFileName
                    };
                    cmdList.clear();
                    cmdList.add("./360VideosMetadata.py");
                    for (String arg : argArray3)
                        cmdList.add(arg);
                    runProcessViaProcessBuilder(cmdList);

                    String argArray4[] = {
                            "--upload", HTTP_ROOT + "__" + outputFileName,
                            "--video-description", "11111",
                            "--video-title", "yyyy"
                    };
                    cmdList.clear();
                    cmdList.add("./youtubeworker");
                    for (String arg : argArray4)
                        cmdList.add(arg);
                    runProcessViaProcessBuilder(cmdList);
                }

            } catch (Exception e) {
                log.severe(e.getMessage());
            }
            log.info("children thread for running of run.py at ending\n");
            log.close();
        }

        private void runProcessViaProcessBuilder(List<String> cmdList) {
            boolean runPyStarted = false;
            ProcessBuilder ps = new ProcessBuilder(cmdList);//cmdList//argArray
            Process pr;
            try {
//                String fileID = HTTP_ROOT + uid + ".txt";

//                HttpServer._mutex.lock();
//                writeToFile(fileID, "2");
//                HttpServer.uid_fileName.put(uid, "2");
//                AppendLineToFile(HttpServer.STATE_INFO, uid + HttpServer.SEPARATOR + "2");
//                HttpServer._mutex.unlock();
//
//                String argArray[] = {
//                        "./run.py",
//                        "-i", HTTP_ROOT + fileName,
//                        "-d", HTTP_ROOT,
//                        "-o", HTTP_ROOT + outputFileName,
//                        "-n", title,
//                        "-t", description,
//                        "-p", profile,
//                        "-u", uid};
//                String args = "";
//                for (int i = 0; i < argArray.length; i++) {
//                    args += argArray[i];
//                    if (i < argArray.length - 1)
//                        args += ",";
//                }
//                log.info("runProcessViaProcessBuilder:argumentsList:" + args);
                ps.redirectErrorStream(true);
                log_time_report.info(uidCut + ":start run.py" + cmdList.toString());
                log.info("starting of run.py " + cmdList.toString());
                pr = ps.start();
                runPyStarted = true;

                BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                log.info("logging of run.py started");
//                MyLogger scriptLog = new MyLogger();
//                scriptLog.open(InstallHandler.class.getName());
                NonBlockingBufferedReader outerScriptLog = new NonBlockingBufferedReader(InstallHandler.class.getName(), in);
                lastPercent = 0;
                last_date = Calendar.getInstance().getTime();
                String exitValue = "";
                outerScriptLog.start();
                do {
//                    tryToReadFrom(in, scriptLog, 1000);

                    JSONObject obj = getJsonObjectFromFile(HTTP_ROOT + uid + ".txt", log);
                    if (isTimeOut(obj)) {
                        log.warning("time out: percent updating too slow");

                        pr.getInputStream().close();
                        pr.getOutputStream().close();
                        pr.getErrorStream().close();
                        pr.destroy();

                        obj.put(STATE_OPTION, SERVER_ERROR);
                        obj.put(ERROR_CODE, SERVER_STITCHING_ERROR);
                        obj.put(DESCRIPTION_OPTION, "Stitching dead at " + lastPercent + " percent");
                        saveByJsonToFile(obj, HTTP_ROOT + uid + ".txt", log);
                        break;
                    } else if (outerScriptLog.isFinished()) {
                        pr.getInputStream().close();
                        pr.getOutputStream().close();
                        pr.getErrorStream().close();
                        pr.destroy();
                        break;
                    } else {
                        Thread.sleep(100);
                    }

                    try { exitValue = "" + pr.exitValue(); }
                    catch (Exception e) { exitValue = ""; }

                } while (exitValue.length() == 0);
//                scriptLog.close(InstallHandler.class.getName());
//                try { outerScriptLog.wait(1); }
//                catch (Exception e) { }
                log.info("logging of run.py ended. waiting for run.py ending");
                int i = pr.waitFor();
                log.info("run.py ended with return=" + i);
                log_time_report.info(uidCut + ":end run.py with return=" + i);
                if ((exitValue.length() != 0)) {
                    pr.getInputStream().close();
                    pr.getOutputStream().close();
                    pr.getErrorStream().close();
                    pr.destroy();
                }
                in.close();
            } catch (Exception e) {
                log.severe(e.getMessage());
                log_time_report.severe(uidCut + ":processing error\n");
            }
            log_time_report.info(uidCut + ":stop handling " + outputLogName + "\n");
            JSONObject obj = getJsonObjectFromFile(HTTP_ROOT + uid + ".txt", log);
            if (obj != null) {
                if (obj.opt(STATE_OPTION).equals(WAITING_STATE)) {
                    obj.put(STATE_OPTION, SERVER_ERROR);
                    obj.put(ERROR_CODE, RUN_EXECUTING_ERROR);
                    saveByJsonToFile(obj, HTTP_ROOT + uid + ".txt", log);
                }
            }
//            log_time_report.close();
        }

        private void tryToReadFrom(BufferedReader in, MyLogger log, int maxLines) {
            String line = "";
            int count = 0;
            char cbuf[] = new char[1000];
            try {
                while (in.ready() && count < maxLines) {
                    if ((in.read(cbuf, 0, 1000)) > 0) {
                        log.script(line);
                    } else {
                        break;
                    }
                    count++;
                }
            } catch (Exception e) {
                ;
            }
        }

        private boolean isTimeOut(JSONObject obj) {
            java.util.Date current_date = Calendar.getInstance().getTime();
            if (obj != null) {
                double currentPercent;
                try { currentPercent = obj.getDouble(PERCENT_OPTION); }
                catch (Exception e) { currentPercent = lastPercent; }

                if (Math.abs(currentPercent - lastPercent) > 0.000001) {
                    lastPercent = currentPercent;
                    last_date = Calendar.getInstance().getTime();
                }
            }
            if (TimeUnit.MILLISECONDS.toSeconds(current_date.getTime() - last_date.getTime()) > 60) {
                return true;
            }
            return false;
        }

        private void runProcessViaRuntimeExec(List<String> cmdList) {
            try {
                String fileID = HTTP_ROOT + uid + ".txt";

                HttpServer._mutex.lock();
                writeToFile(fileID, "2");
//                HttpServer.uid_fileName.put(uid, "2");
                AppendLineToFile(HttpServer.STATE_INFO, uid + HttpServer.SEPARATOR + "2");
                HttpServer._mutex.unlock();

//                Process pr = Runtime.getRuntime().exec(this.command);
                Process pr = Runtime.getRuntime().exec(cmdList.toArray(new String[cmdList.size()]));

                BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    log.script(line);
                }
                int i = pr.waitFor();
                log.info("ok! status=" + i);

                HttpServer._mutex.lock();
                String status = i > 0 ? "0" : "1";
                HttpServer.uid_fileName.put(uid, status);
                AppendLineToFile(HttpServer.STATE_INFO, uid + HttpServer.SEPARATOR + status);
                writeToFile(fileID, (i > 0 ? "0" : "1"));
                log.info("status added! status=" + i);
                HttpServer._mutex.unlock();

                in.close();
            } catch (Exception e) {
                log.severe(e.getMessage());
            }
        }

        public void setParams(String _folderToSave,
                              String _uid,
                              String _title,
                              String _description,
                              String _profile,
                              String _command,
                              String _fileName,
                              String _outputFile,
                              String _outputLogName) {
//            this.folderToSave = _folderToSave;
//            uid = _uid;
//            title = _title;
//            description = _description;
//            profile = _profile;
//            command = _command;
//            fileName = _fileName;
//            outputFileName = _outputFile;
//            outputLogName = _outputLogName;
        }
    }

    private String getFolder(String fileName) {
        return fileName;
    }

}