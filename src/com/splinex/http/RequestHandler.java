package com.splinex.http;//package com.splinex.http;

//import android.content.Context;

import static fi.iki.elonen.NanoHTTPD.IHTTPSession;
import static fi.iki.elonen.NanoHTTPD.Response;

public abstract class RequestHandler {
    private String name;
//    protected Context context;

    public RequestHandler(String name) {
        this.name = name;
    }

    public abstract Response handle(IHTTPSession session);

    public String getName() {
        return name;
    }

//    public void setContext(Context context) {
//        this.context = context;
//    }
}