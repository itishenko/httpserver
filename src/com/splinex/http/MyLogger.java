package com.splinex.http;

import com.sun.imageio.stream.StreamCloser;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by human on 3/27/15.
 */
public class MyLogger {
//    private Logger logger = Logger.getLogger("HttpServerLog");
//    private FileHandler fh;
    private FileWriter fw;
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private String fileName = "MyLogger.txt";
    public static HashMap<String, Integer> counter = new HashMap<String, Integer>();
    public static Lock counterMutex = new ReentrantLock(true);

    void open(String _fileName) {
//        fileName = _fileName;
        try {
            fileName = _fileName;
            incCounterFor(fileName);
            fw = new FileWriter(fileName, true);
            // This block configure the logger with handler and formatter
//            fh = new FileHandler(fileName);
//            logger.addHandler(fh);
//            SimpleFormatter formatter = new SimpleFormatter();
//            fh.setFormatter(formatter);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void close() {
        try {
            if (decCounterFor(fileName) == true)
                fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void close(String _fileName) {
        try {
            if (decCounterFor(_fileName) == true)
                fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void incCounterFor(String fileName) {
        counterMutex.lock();
        if (counter.get(fileName) == null)
            counter.put(fileName, 1);
        else
            counter.put(fileName, counter.get(fileName) + 1);
        counterMutex.unlock();
    }
    boolean decCounterFor(String fileName) {
        counterMutex.lock();
        if (counter.get(fileName) == null)
            System.err.println("\nERROR: can't close " + fileName + " not found\n");
        else
            counter.put(fileName, counter.get(fileName) - 1);
        counterMutex.unlock();
        if (counter.get(fileName) == 0)
            return true;
        if (counter.get(fileName) < 0)
            System.err.println("\n" + dateFormat.format(Calendar.getInstance().getTime())
                    + "ERROR: can't close " + fileName + " " + counter.get(fileName) + " unexpected error\n");
        return false;
    }

    // the following statement is used to log any messages
    void info (String message) {
//        Date date = new Date();
        AppendLineToFile(dateFormat.format(Calendar.getInstance().getTime()) + ":INFO:" + message);
//        logger.fine(message);
    }
    void severe (String message) {
//        Date date = new Date();
        AppendLineToFile(dateFormat.format(Calendar.getInstance().getTime()) + ":SEVERE:" + message);
//        logger.fine(message);
    }
    void warning (String message) {
//        Date date = new Date();
        AppendLineToFile(dateFormat.format(Calendar.getInstance().getTime()) + ":WARNING:" + message);
//        logger.fine(message);
    }
    void script (String message) {
//        Date date = new Date();
        AppendLineToFile(dateFormat.format(Calendar.getInstance().getTime()) + ":OUTER SCRIPT:" + message);
//        logger.fine(message);
    }

    private boolean AppendLineToFile(String text) {
        try
        {
            fw.write(text + "\n");//appends the string to the file
            fw.flush();
            return true;
        }
        catch (StreamCorruptedException e)
        {
            System.err.println("StreamCorruptedException" + counterMutex.toString() + " " + counter.get(fileName));
        }
        catch(IOException ioe)
        {
            System.err.println(ioe.getMessage() + " " + counterMutex.toString()
                    + " counter=" + counter.get(fileName) + " " + fileName + " " + counter.toString() + "\n" + text);
//            ioe.printStackTrace();
            if (ioe.getMessage().matches("Stream closed"))
                open(fileName);
        }
        catch (Exception e) {
            System.err.println("unknown error:" + e.getMessage() + " " + counterMutex.toString()
                    + " counter=" + counter.get(fileName) + " " + fileName + " " + counter.toString());
        }
        return false;
    }

    public void open() {
        open(fileName);
    }
}
