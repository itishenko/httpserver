package com.splinex.http;//package com.splinex.http;

//import android.content.Context;
import fi.iki.elonen.NanoHTTPD;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import java.util.HashMap;

public class HttpServer extends NanoHTTPD {
    private HashMap<String, RequestHandler> handlers = new HashMap<String, RequestHandler>();
    private RequestHandler fileHandler;
    public static Lock _mutex = new ReentrantLock(true);
    public static final String SEPARATOR = " ";
    public static final String STATE_INFO = "state_info";
    public static HashMap<String, String> uid_fileName = new HashMap<String, String>();
//    private Context context;

    public HttpServer(int port) {
        super(port);
//        this.context = context;
        fileHandler = new HttpFileHandler();
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        if ("/".equals(uri))
            return redirect("index.html");
        if (handlers.containsKey(uri))
            return handlers.get(uri).handle(session);
        Response response = fileHandler.handle(session);
        return response != null ? response : super.serve(session);
    }

    public void registerHandler(RequestHandler handler) {
//        handler.setContext(context);
        handlers.put(handler.getName(), handler);
    }

    public void registerHandlers(RequestHandler... handlers) {
        for (RequestHandler handler : handlers)
            registerHandler(handler);
    }

    private Response redirect(String target) {
        Response response = new Response(Response.Status.REDIRECT, "", "");
        response.addHeader("Location", target);
        return response;
    }

}