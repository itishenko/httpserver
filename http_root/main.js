function asyncRequest(path, onResponse, onError, data){
    var xmlhttp=window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
            if (onResponse !== undefined)
                onResponse(xmlhttp.responseText)
        }
        else
            if (onError !== undefined)
                onError(xmlhttp.readyState, xmlhttp.status)
    }
    if (data !== undefined) {
        xmlhttp.open("POST",path,true);
        xmlhttp.send(data);
    }
    else {
        xmlhttp.open("GET",path,true);
        xmlhttp.send(data);
    }


}

function get(id) {return document.getElementById(id);}
function show(el) {el.style.display="block";}
function hide(el) {el.style.display="none";}

var counter=60;
function updateCounter()
{
    if (--counter<0)
    {
        clearInterval(ct);
        redirect();
    }else
        info.innerText="Install OK. Rebooting... Please wait for "+counter+" seconds";
}

function redirect() {
    window.location.replace(base);
}

function redirectAfterBoot()
{
    asyncRequest(window.location,redirect,
        function(){
            setTimeout(redirectAfterBoot,2000);
        });
}

function install(data)
{
    show(info);
    info.innerText="Installing..."
    var form=new FormData();
    form.append("file",data);
    asyncRequest("install", function(){
        hide(interface);
        info.innerText="Install OK. Rebooting... Please wait for "+counter+" seconds";
        setTimeout(redirectAfterBoot,5000);
        ct=setInterval(updateCounter,1000);
    },
    function(){
        info.innerText="";
    },form);
}

function onInstallClick()
{
    var files=uploadFile.files;
    if (files.length==0) {
        alert("Choose file");
        return;
    }
    install(files[0])
}

function receiveMessage(event){
  var obj=event.data;
  if (obj.data)
    install(new Blob([obj.data]));
}

function onLoad(){
    base="http://"+window.location.hostname+":4000";
    window.addEventListener("message", receiveMessage, false);
    install.onclick=onInstallClick;
    back.href=base;
}
