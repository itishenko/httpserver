#!/usr/bin/python
from glob import iglob
import sys, getopt
import shutil
import os

def concat(chunks, outfile, dirpath):
    input_chunks = open(chunks, 'r')
    destination = open(outfile, 'wb')
    for chunk in input_chunks.readlines():
        filename=chunk.strip()	
        shutil.copyfileobj(open(dirpath+filename, 'rb'), destination)
    destination.close()
    sys.exit(0)
    
def main(argv):
   inputfile = ''
   outputfile = ''
   dirpath = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:d:",["ifile=","ofile=","dir="])
   except getopt.GetoptError:
      print 'concat.py -i <inputfile> -d <path> -o <outputfile>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'concat.py -i <inputfile> -o <outputfile>'
         sys.exit(2)
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-d", "--dir"):
         dirpath = arg
   if(inputfile=='' or outputfile==''):
      print 'concat.py -i <inputfile> -d <path> -o <outputfile>'
      sys.exit(2)
   concat(inputfile, outputfile, dirpath)
   	
if __name__ == "__main__":
   main(sys.argv[1:])
