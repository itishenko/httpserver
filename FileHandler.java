package com.splinex.http;

//import android.content.Context;
//import android.content.res.AssetManager;
//import android.webkit.MimeTypeMap;
//import com.splinex.streaming.Log;
import fi.iki.elonen.NanoHTTPD;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FileHandler extends RequestHandler {
    public static final String HTTP_ROOT = "http_root";
    public static final String UID = "uid";

    public FileHandler() {
        super("");
    }

    @Override
    public NanoHTTPD.Response handle(NanoHTTPD.IHTTPSession session) {
        String filename = session.getUri();
//        AssetManager assets = context.getAssets();
        String sQPS = session.getQueryParameterString();
        String sHdr = session.getHeaders().toString();
        String sMtd = session.getMethod().toString();
        String sInpStr = session.getInputStream().toString();
        String sPrm = session.getParms().toString();
        String sUri = session.getUri();
        System.out.print("\nFH\n\nQueryParameterString:\n" + sQPS
                + "\n\nHeaders:\n" + sHdr
                + "\n\nMethod\n" + sMtd
                + "\n\nInputStream\n" + sInpStr
                + "\n\nParams\n" + sPrm
                + "\n\nUri\n" + sUri
                + "\n\n" + session.getCookies().toString()
                + "\n\n" + session.toString());
        try {
//            InputStream is = assets.open(HTTP_ROOT + filename);
            if (session.getMethod() == NanoHTTPD.Method.GET && session.getUri().equals("/state_info")) {
                if (session.getHeaders().get(UID) != null) {//
                    String uid = session.getHeaders().get(UID);
                    String statusFileName = findFileStatus(HTTP_ROOT, uid);
                    return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK,
                            HttpServer.MIME_PLAINTEXT, "file with uid=" + uid);
                }
                else {// get full list
                    String statusFileName = findFilesStatus(HTTP_ROOT);
                    return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK,
                            HttpServer.MIME_PLAINTEXT, statusFileName);
                }
            }
            else {// saving file
                InputStream is = new FileInputStream(HTTP_ROOT + filename);
                String mime = getMime(filename);
                System.out.print(filename + " => " + mime);
                return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, mime, is);
            }
        } catch (FileNotFoundException e) {
            System.err.print("File not found: " + filename);
        } catch (IOException e) {
            System.err.print("IOException: " + e);
        }

        return null;
    }

    private String findFilesStatus(String httpRoot) {
        String result = "";
        File f = new File(httpRoot);
        String[] list = f.list();     //список файлов в текущей папке
        for (String file : list) {
            result += file + "//";
        }
        System.out.print(result);
        return result;
    }

    private String findFileStatus(String httpRoot, String uid) {
        System.out.print("function not released - return file status with uid" + uid);
        return ("function not released - return file status with uid" + uid);
    }

    private String getMime(String filename) {
        int idx = filename.lastIndexOf(".");
        String ext = filename.substring(idx + 1);
        return "js".equals(ext) ? "text/javascript" :
                "text/plain";
    }
}